package com.citi.training.customer.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTests {

    private int testId = 23;
    private String testName = "Spring Beans";
    private String testAddress = "45 FalseAddress Avenue";

    @Test
    public void test_Customer_constructor() {
        Customer testCustomer = new Customer(testId, testName, testAddress);;

        assertEquals(testId, testCustomer.getId());
        assertEquals(testName, testCustomer.getName());
        assertEquals(testAddress, testCustomer.getAddress(), 0.0001);
    }

    @Test
    public void test_Product_toString() {
        String testString = new Customer(testId, testName, testAddress).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains((new String(testAddress).toString())));
    }
}
