package com.citi.training.customer.model;


import java.util.concurrent.atomic.AtomicInteger;

public class Customer {

    // an integer that is incremented with each new employee
    private static AtomicInteger idGenerator = new AtomicInteger(0);

    private int id;
    private String Name;
    private String Address;
    
    // Constructors. Note, no default constructor.
    public Customer(int id,String Name, String Address) {
    	this.id = id;
		this.Name = Name;
		this.Address = Address;
    }
   


	public static AtomicInteger getIdGenerator() {
		return idGenerator;
	}


	public static void setIdGenerator(AtomicInteger idGenerator) {
		Customer.idGenerator = idGenerator;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getAddress() {
		return Address;
	}


	public void setAddress(String address) {
		Address = address;
	}

	
	 @Override
	    public String toString() {
	        return "Customer [id=" + id + ", Name=" + Name + ", Address=" + Address;
	    }
}
