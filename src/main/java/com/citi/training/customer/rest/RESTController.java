package com.citi.training.customer.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.customer.model.Customer;
//import com.citi.training.customer.model.Product;
import com.citi.training.customer.repo.DOARepo;

@RestController
@RequestMapping("/customer")
public class RESTController {
	private static Logger LOG = LoggerFactory.getLogger(RESTController.class);
	@Autowired
	private DOARepo customerRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Customer> getAll(){
		LOG.info("getAll was called");
		LOG.debug("This is a debug message");
		return customerRepository.getAllCustomer();
	}
	@RequestMapping(method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> save(@RequestBody Customer customer) {
		LOG.info("save was called" + customer);
		customerRepository.saveCustomer(customer);
		return new ResponseEntity<Customer>(customer,HttpStatus.CREATED);
	}
	@RequestMapping(value="{id}",
			method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public Customer get(@PathVariable int id) {
		LOG.info("get was called, id: "+id);
		return customerRepository.getCustomer(id);
	}
	@RequestMapping(value="{id}",method=RequestMethod.DELETE)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int id){
		LOG.info("deleteCustomer was called, id: "+id);
		//System.out.println("deleteProduct was called, id: "+id);
		customerRepository.deleteCustomer(id);
		//System.out.println("Remaining products are: ");
		//return customerRepository.getAllCustomers();
		//LOG.debug("This is a debug message");
		//return productRepository.getAllProducts();
	}
}
