package com.citi.training.customer.repo;

import java.util.List;

import com.citi.training.customer.model.Customer;


public interface DAOInterface {

	 void saveProduct(Customer product);

	    Customer getProduct(int id);

	    List<Customer> getAllProducts();
	    
	    void deleteCustomer(int id);
}

