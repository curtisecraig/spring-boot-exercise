package com.citi.training.customer.repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.customer.model.Customer;

@Component
public class DOARepo {

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();
	  

	  public void saveCustomer(Customer customer) {
	      allCustomers.put(customer.getId(), customer);
	  }

	  public Customer getCustomer(int id) {
	  	
	  	  Customer cust = allCustomers.get(id);
//	  	  if (p == null) {
//	  		  throw new ProductNotFoundException("Product was not found: " + id);
//	  	  }
	      
	     
	      return cust;
	  }

	  public List<Customer> getAllCustomer() {
	      return new ArrayList<Customer>(allCustomers.values());
	  }
	  
	  public void deleteCustomer(int id) {
	  	allCustomers.remove(id);
//	  	if (p == null) {
//	  		throw new ProductNotFoundException("The product was not found: " + id);
//	  	}
	  	
}
}
